<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PeriodoController extends Controller{

    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request){
        $userID = 0;
        $gestionID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idGestion')){
                $gestionID = $request->session()->get('idGestion');
            }
            if($request->has('insert')){
                if($request->insert == 'true'){
                    $name = '';
                    $fIni = '';
                    $fFin = '';
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    if($request->has('fIni')){
                        $fIni = $request->fIni;
                    }
                    if($request->has('fFin')){
                        $fFin = $request->fFin;
                    }
                    return $this->insert($name, $fIni, $fFin, $userID, $gestionID);
                }
            }
            if($request->has('update')){
                if($request->update == 'true'){
                    $name = '';
                    $fIni = '';
                    $fFin = '';
                    $periodoID = 0;
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    if($request->has('fIni')){
                        $fIni = $request->fIni;
                    }
                    if($request->has('fFin')){
                        $fFin = $request->fFin;
                    }
                    if($request->has('periodoID')){
                        $periodoID = $request->periodoID;
                    }
                    return $this->update($name, $fIni, $fFin, $userID, $gestionID, $periodoID);
                }
            }
            if($request->has('delete')){
                if($request->delete == 'true'){
                    $periodoID = 0;
                    if($request->has('periodoID')){
                        $periodoID = $request->periodoID;
                    }
                    return $this->delete($periodoID, $userID);
                }
            }
            $query = "SELECT Idperiodo as ID, Nombre, DATE_FORMAT(FechaInicio, '%d/%m/%Y') as 'Fecha Inicio', DATE_FORMAT(FechaFin, '%d/%m/%Y') as 'Fecha Fin', CASE Estado WHEN 0 THEN 'Abierto' WHEN 1 THEN 'Cerrado' END as Estado FROM periodo WHERE (Estado=0 OR Estado=1)  AND IdUsuario=? AND IdGestion=? ORDER BY IdPeriodo DESC";
            $data=[$userID, $gestionID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function insert($name, $fIni, $fFin, $userID, $gestionID){
        $inspectQuery = "SELECT FechaInicio as fini, FechaFin as ffin FROM gestion WHERE IdUsuario=? AND IdGestion=? LIMIT 1";
        $inspectData = [$userID, $gestionID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 1){
            $inspectResult = $inspectResult[0];
            if(($fIni >= $inspectResult->fini && $fIni <= $inspectResult->ffin) && ($fFin >= $inspectResult->fini && $fFin <= $inspectResult->ffin)){
                $inspectQuery = "SELECT IdPeriodo FROM periodo WHERE Nombre=? AND IdUsuario=? AND IdGestion=?";
                $inspectData = [$name, $userID, $gestionID];
                $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                if(count($inspectResult) == 0){
                    $inspectQuery = "SELECT FechaInicio as fini, FechaFin as ffin FROM periodo WHERE IdUsuario=? AND IdGestion=?";
                    $inspectData = [$userID, $gestionID];
                    $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                    foreach($inspectResult as $value){
                        if(($fIni >= $value->fini && $fIni <= $value->ffin) || ($fFin >= $value->fini && $fFin <= $value->ffin) || ($fIni < $value->fini && $fFin > $value->ffin)){
                            return response()->json(['Success'=>false, 'content'=>'No puede existir solapamiento entre periodos']);
                        }
                    }
                    $query = "INSERT INTO periodo (Nombre, FechaInicio, FechaFin, Estado, IdUsuario, IdGestion) VALUES(?,?,?,0,?,?)";
                    $data = [$name, $fIni, $fFin, $userID, $gestionID];
                    $result = app('App\Http\Controllers\DBController')->insert($query, $data);
                    return response()->json(['Success'=>true, 'content'=>$result]);
                }
                return response()->json(['Success'=>false, 'content'=>'El nombre esta ocupado por un periodo']);
            }
            return response()->json(['Success'=>false, 'content'=>'El periodo debe permanecer en el rango de la gestión']);
        }
        return response()->json(['Success'=>false, 'content'=>'No existe la gestión']);
    }
    public function update($name, $fIni, $fFin, $userID, $gestionID, $periodoID){
        $inspectQuery = "SELECT FechaInicio as fini, FechaFin as ffin FROM gestion WHERE IdUsuario=? AND IdGestion=? LIMIT 1";
        $inspectData = [$userID, $gestionID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 1){
            $inspectResult = $inspectResult[0];
            if(($fIni >= $inspectResult->fini && $fIni <= $inspectResult->ffin) && ($fFin >= $inspectResult->fini && $fFin <= $inspectResult->ffin)){
                $inspectQuery = "SELECT IdPeriodo FROM periodo WHERE Nombre=? AND IdUsuario=? AND IdGestion=? AND IdPeriodo!=?";
                $inspectData = [$name, $userID, $gestionID, $periodoID];
                $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                if(count($inspectResult) == 0){
                    $inspectQuery = "SELECT IdPeriodo FROM periodo WHERE IdUsuario=? AND IdGestion=? AND IdPeriodo=? AND Estado=0 LIMIT 1";
                    $inspectData = [$userID, $gestionID, $periodoID];
                    $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                    if(count($inspectResult) == 1){
                        $inspectQuery = "SELECT FechaInicio as fini, FechaFin as ffin FROM periodo WHERE IdUsuario=? AND IdGestion=? AND IdPeriodo!=?";
                        $inspectData = [$userID, $gestionID, $periodoID];
                        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                        foreach($inspectResult as $value){
                            if(($fIni >= $value->fini && $fIni <= $value->ffin) || ($fFin >= $value->fini && $fFin <= $value->ffin) || ($fIni < $value->fini && $fFin > $value->ffin)){
                                return response()->json(['Success'=>false, 'content'=>'No puede existir solapamiento entre periodos']);
                            }
                        }
                        $query = "UPDATE periodo SET Nombre=?, FechaInicio=?, FechaFin=? WHERE IdUsuario=? AND IdGestion=? AND IdPeriodo=?";
                        $data = [$name, $fIni, $fFin, $userID, $gestionID, $periodoID];
                        $result = app('App\Http\Controllers\DBController')->update($query, $data);
                        return response()->json(['Success'=>true, 'content'=>$result]);
                    }
                    return response()->json(['Success'=>false, 'content'=>'No se puede modificar, el periodo esta cerrada']);
                }
                return response()->json(['Success'=>false, 'content'=>'El nombre esta ocupado por un periodo']);
            }
            return response()->json(['Success'=>false, 'content'=>'El periodo debe permanecer en el rango de la gestión']);
        }
        return response()->json(['Success'=>false, 'content'=>'No existe la gestión']);
    }

    public function delete($periodoID, $userID){
        $query = "DELETE FROM periodo WHERE IdPeriodo=? AND IdUsuario=?";
        $data = [$periodoID, $userID];
        $result = app('App\Http\Controllers\DBController')->delete($query, $data);
        return response()->json(['Success'=>true, 'content'=>$result]);
    }

    public function getColumnName(){
        if(Auth::check()){
            $result = ['ID', 'Nombre', 'Fecha Inicio', 'Fecha Fin', 'Estado'];
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }

    public function transaction(Request $request){
        return app('App\Http\Controllers\DBController')->transaction([$this, 'select'], $request);  
    }
}