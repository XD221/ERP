<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PlanCuentaController extends Controller{
    public function index(Request $request){
        function children(&$children, $data, $fatherID, $limit){
            $result = false;
            for($e = 0; $e < count($children); $e++){
                $d = $children[$e];
                if($fatherID != $d['id']){
                    if(array_key_exists('children', $children[$e]) AND $d['level'] < $limit){
                        children($children[$e]['children'], $data, $fatherID, $limit);
                    }else{
                        continue;
                    }
                }else{
                    if($d['level'] <= $limit){
                        if(array_key_exists('children', $children[$e])){
                            array_push($children[$e]['children'], $data);
                            $children[$e]['delete'] = false;
                        }else{
                            $children[$e]['children'] = [];
                            array_push($children[$e]['children'], $data);
                            $children[$e]['delete'] = false;
                        }
                    }
                    break;
                    $result = true;
                }
            }
            //var_dump($children);
            return $result;
        }
        $company = app('App\Http\Controllers\DBConsult\EmpresaController')->selectById($request->session()->get('idEmpresa'), Auth::user()->id);
        $company = json_decode(json_encode($company->getData()), true);
        $lvlCompany = 0;
        if($company['Success']){
            if(count($company['content']) > 0){
                $lvlCompany = $company['content']['Niveles'];
                $result = app('App\Http\Controllers\DBConsult\CuentaController')->select($request);
                $result = json_decode(json_encode($result->getData()), true);
                $data = [];
                if($result['content']){
                    if(count($result['content']) > 0){
                        //var_dump($result['content']);
                        foreach($result['content'] as $r){
                            $newData = array(
                                'id'=> $r['id'],
                                'code'=> $r['Codigo'],
                                'name'=> $r['Nombre'],
                                'level'=>$r['Nivel'],
                                'addTreeNodeDisabled'=> true,
                                'addLeafNodeDisabled'=> true,
                                'editNodeDisabled'=> true,
                                'delNodeDisabled'=> true,
                                'delete'=> true,
                                'dragDisabled'=> true,
                            );
                            if($r['Nivel'] == 1){
                                array_push($data, $newData);
                                continue;
                            }
                            if($r['Nivel'] > 1){
                                for($i = 0; $i < count($data); $i++){
                                    $d = $data[$i];
                                    if($r['padreID'] != $d['id']){
                                        if(array_key_exists('children', $data[$i])){
                                            //echo "<br><br>";
                                            //echo $r['padreID']." - ".$d['id']." -- ".$d['code']."<br>";
                                            //echo "<br>---------------------------<br>";
                                            //var_dump($data[$i]);
                                            //echo "<br>===========================<br>";
                                            //var_dump($r);
                                            //echo "<br>---------------------------<br>";
                                            if(children($data[$i]['children'], $newData, $r['padreID'], $lvlCompany)){
                                                break;
                                            }
                                        }else{
                                            continue;
                                        }
                                    }else{
                                        if(array_key_exists('children', $data[$i])){
                                            array_push($data[$i]['children'], $newData);
                                            $data[$i]['delete'] = false;
                                        }else{
                                            $data[$i]['children'] = [];
                                            array_push($data[$i]['children'], $newData);
                                            $data[$i]['delete'] = false;
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        //echo "<br><br><br><br><br>";
        //return var_dump($data);
        return view('PlanCuenta.Index',[
            'data'=>$data,
            'limit'=>$lvlCompany,
            'url_report'=>'http://localhost:8080/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2FERP_report&reportUnit=%2FERP_report%2Fcuenta_report&standAlone=true&j_username=cuenta_report&j_password=1234&sessionDecorator=no&idUsuario='.Auth::user()->id.'&idEmpresa='.$request->session()->get('idEmpresa'),
        ]);
    }
}