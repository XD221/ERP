<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CuentaController extends Controller{

    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request){
        $userID = 0;
        $companyID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            if($request->has('insert')){
                if($request->insert == 'true'){
                    $name = '';
                    $code = '';
                    $level = 1;
                    $accountType = 1;
                    $fatherID = null;
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    if($request->has('code')){
                        $code = $request->code;
                    }
                    if($request->has('level')){
                        $level = $request->level;
                    }
                    if($request->has('accountType')){
                        $accountType = $request->accountType;
                    }
                    if($request->has('fatherID')){
                        $fatherID = $request->fatherID;
                    }
                    return $this->insert($name, $code, $level, $accountType, $fatherID, $userID, $companyID);
                }
            }
            if($request->has('update')){
                if($request->update == 'true'){
                    $cuentaID = 0;
                    $name = '';
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    if($request->has('cuentaID')){
                        $cuentaID = $request->cuentaID;
                    }
                    return $this->update($name, $userID, $companyID, $cuentaID);
                }
            }
            if($request->has('delete')){
                if($request->delete == 'true'){
                    $cuentaID = 0;
                    if($request->has('cuentaID')){
                        $cuentaID = $request->cuentaID;
                    }
                    return $this->delete($userID, $companyID, $cuentaID);
                }
            }
            $query = "SELECT IdCuenta as id, Codigo, Nombre, Nivel, TipoCuenta, IdCuentaPadre as padreID FROM cuenta WHERE IdUsuario=? AND IdEmpresa=? ORDER BY Nivel ASC, id ASC";
            $data=[$userID, $companyID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function insert($name, $code, $level, $accountType, $fatherID, $userID, $companyID){
        $inspectQuery = "SELECT * FROM cuenta WHERE Nombre=? AND IdUsuario=? AND IdEmpresa=?";
        $inspectData = [$name, $userID, $companyID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            //$query = "INSERT INTO cuenta (Codigo, Nombre, Nivel, TipoCuenta, IdUsuario, IdEmpresa, IdCuentaPadre) VALUES(?,?,?,?,?,?,?)";
            $data = ['Codigo'=>$code, 'Nombre'=>$name, 'Nivel'=>$level, 'TipoCuenta'=>$accountType, 'IdUsuario'=>$userID, 'IdEmpresa'=>$companyID, 'IdCuentaPadre'=>$fatherID];
            $result = app('App\Http\Controllers\DBController')->insertGetId('cuenta', $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'El nombre esta ocupado por otra cuenta']);
    }
    public function update($name, $userID, $companyID, $cuentaID){
        $inspectQuery = "SELECT * FROM cuenta WHERE Nombre=? AND IdUsuario=? AND IdEmpresa=? AND IdCuenta!=?";
        $inspectData = [$name, $userID, $companyID, $cuentaID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            $query = "UPDATE cuenta SET Nombre=? WHERE IdUsuario=? AND IdEmpresa=? AND IdCuenta=?";
            $data = [$name, $userID, $companyID, $cuentaID];
            $result = app('App\Http\Controllers\DBController')->update($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'El nombre esta ocupado por otra cuenta']);
    }

    public function delete($userID, $companyID, $cuentaID){
        $inspectQuery = "SELECT * FROM cuenta WHERE IdUsuario=? AND IdEmpresa=? AND IdCuentaPadre=? AND IdCuentaPadre!=null";
        $inspectData = [$userID, $companyID, $cuentaID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            $query = "DELETE FROM cuenta WHERE IdUsuario=? AND IdEmpresa=? AND IdCuenta=?";
            $data = [$userID, $companyID, $cuentaID];
            $result = app('App\Http\Controllers\DBController')->update($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }else{
            return response()->json(['Success'=>false, 'content'=>'No se puede eliminar la cuenta']);
        }
    }

    public function transaction(Request $request){
        return app('App\Http\Controllers\DBController')->transaction([$this, 'select'], $request);  
    }
}