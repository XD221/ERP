@extends('Layouts.Basic')
@section('head')
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<style>
    html, body {
        background-color: #152036;
        height: 100%;
    }
</style>
@endsection
@section('body')
<div class="h-100 row align-items-center" id='content'>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center text-light">
                <h2>Acceso no autorizado</h2>
                <h3>Recargue la pagina para volver a loguear</h3>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
<script type="text/javascript">
    $('#frame').css("height", 0);
    $('#frame').css("height", $(document).height());
</script>
@endsection