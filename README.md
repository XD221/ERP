CREATE TABLE Usuario(
	id INT AUTO_INCREMENT,
    Nombre VARCHAR(35) NOT NULL,
    Usuario VARCHAR(25) NOT NULL,
    password VARCHAR(255) NOT NULL,
    Tipo TINYINT NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE Empresa(
	IdEmpresa INT AUTO_INCREMENT,
    Nombre VARCHAR(65) NOT NULL,
    NIT VARCHAR(13) NOT NULL,
    Sigla VARCHAR(8) NOT NULL,
    Telefono VARCHAR(18) NOT NULL,
    Correo VARCHAR(85) NOT NULL,
    Direccion VARCHAR(360) NOT NULL,
    Niveles INT NOT NULL,
    Estado TINYINT NOT NULL,
    IdUsuario INT NOT NULL,
    PRIMARY KEY(IdEmpresa),
    FOREIGN KEY(IdUsuario) REFERENCES Usuario(id)
);

CREATE TABLE Gestion(
	IdGestion INT AUTO_INCREMENT,
    Nombre VARCHAR(35) NOT NULL,
    FechaInicio DATE NOT NULL,
    FechaFin DATE NOT NULL,
    Estado TINYINT NOT NULL,
    IdUsuario INT NOT NULL,
    IdEmpresa INT NOT NULL,
    PRIMARY KEY(IdGestion),
    FOREIGN KEY(IdUsuario) REFERENCES Usuario(id),
    FOREIGN KEY(IdEmpresa) REFERENCES Empresa(IdEmpresa)
);

CREATE TABLE Periodo(
	IdPeriodo INT AUTO_INCREMENT,
    Nombre VARCHAR(35) NOT NULL,
    FechaInicio DATE NOT NULL,
    FechaFin DATE NOT NULL,
    Estado TINYINT NOT NULL,
    IdUsuario INT NOT NULL,
    IdGestion INT NOT NULL,
    PRIMARY KEY(IdPeriodo),
    FOREIGN KEY(IdUsuario) REFERENCES Usuario(id),
    FOREIGN KEY(IdGestion) REFERENCES Gestion(IdGestion)
);

CREATE TABLE Cuenta(
	IdCuenta INT AUTO_INCREMENT,
	Codigo VARCHAR(22) NOT NULL,
	Nombre VARCHAR(45) NOT NULL,
	Nivel INT NOT NULL,
	TipoCuenta TINYINT NOT NULL,
	IdUsuario INT NOT NULL,
	IdEmpresa INT NOT NULL,
	IdCuentaPadre INT DEFAULT NULL,
	PRIMARY KEY(IdCuenta),
	FOREIGN KEY(IdUsuario) REFERENCES Usuario(id),
	FOREIGN KEY(IdEmpresa) REFERENCES Empresa(IdEmpresa),
	FOREIGN KEY(IdCuentaPadre) REFERENCES Cuenta(IdCuenta)
);
CREATE TABLE Moneda(
	IdMoneda INT AUTO_INCREMENT,
	Nombre VARCHAR(35) NOT NULL,
	Descripcion VARCHAR(100) NOT NULL,
	Abreviatura VARCHAR(6) NOT NULL,
	IdUsuario INT NOT NULL,
	PRIMARY KEY(IdMoneda),
	FOREIGN KEY(IdUsuario) REFERENCES usuario(id)

);
CREATE TABLE EmpresaMoneda(
	IdEmpresaMoneda INT AUTO_INCREMENT,
	Cambio DOUBLE NOT NULL,
	Activo INT NOT NULL,
	FechaRegistro DATE NOT NULL,
	IdEmpresa INT NOT NULL,
	IdMonedaPrincipal INT NOT NULL,
	IdMonedaAlternativa INT DEFAULT NULL,
	IdUsuario INT NOT NULL,
	PRIMARY KEY(IdEmpresaMoneda),
	FOREIGN KEY(IdEmpresa) REFERENCES empresa(IdEmpresa),
	FOREIGN KEY(IdMonedaPrincipal) REFERENCES moneda(IdMoneda),
	FOREIGN KEY(IdMonedaAlternativa) REFERENCES moneda(IdMoneda),
	FOREIGN KEY(IdUsuario) REFERENCES usuario(id)
);
CREATE TABLE Comprobante(
	IdComprobante INT AUTO_INCREMENT,
	Serie INT NOT NULL,
	Glosa VARCHAR(40) NOT NULL,
	Fecha DATE NOT NULL,
	TC DOUBLE NOT NULL,
	TipoComprobante TINYINT NOT NULL,
	Estado TINYINT NOT NULL,
	IdEmpresa INT NOT NULL,
	IdUsuario INT NOT NULL,
	IdMoneda INT NOT NULL,
	PRIMARY KEY(IdComprobante),
	FOREIGN KEY(IdEmpresa) REFERENCES empresa(IdEmpresa),
	FOREIGN KEY(IdUsuario) REFERENCES usuario(id),
	FOREIGN KEY(IdMoneda) REFERENCES moneda(IdMoneda)
);
CREATE TABLE DetalleComprobante(
	IdDetalleComprobante INT AUTO_INCREMENT,
	Numero INT NOT NULL,
	Glosa VARCHAR(40) NOT NULL,
	MontoDebe DOUBLE NOT NULL,
	MontoHaber DOUBLE NOT NULL,
	MontoDebeAlt DOUBLE NOT NULL,
	MontoHaberAlt DOUBLE NOT NULL,
	IdUsuario INT NOT NULL,
	IdComprobante INT NOT NULL,
	IdCuenta INT NOT NULL,
	PRIMARY KEY(IdDetalleComprobante),
	FOREIGN KEY(IdUsuario) REFERENCES usuario(id),
	FOREIGN KEY(IdComprobante) REFERENCES comprobante(IdComprobante),
	FOREIGN KEY(IdCuenta) REFERENCES cuenta(IdCuenta)
);

//INSERT
INSERT usuario (Nombre,Usuario,password,Tipo) VALUES('Diego', 'Master', '81dc9bdb52d04dc20036dbd8313ed055', 1);

INSERT moneda (Nombre,Descripcion,Abreviatura,IdUsuario) VALUES('Dolar', 'Moneda de los Estados Unidos.', 'USD.', 1);
INSERT moneda (Nombre,Descripcion,Abreviatura,IdUsuario) VALUES('Boliviano', 'Moneda Boliviana.', 'BOB.', 1);
INSERT moneda (Nombre,Descripcion,Abreviatura,IdUsuario) VALUES('Rublo', 'Moneda Rusa.', 'RUB.', 1);
INSERT moneda (Nombre,Descripcion,Abreviatura,IdUsuario) VALUES('Euro', 'Moneda Europea.', 'EUR.', 1);
INSERT moneda (Nombre,Descripcion,Abreviatura,IdUsuario) VALUES('Libra Esterlina', 'Moneda del Reino Unido.', 'GBP.', 1);