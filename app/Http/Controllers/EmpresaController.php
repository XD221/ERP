<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmpresaController extends Controller{
    public function index(Request $request){
        $money = [];
        $result = app('App\Http\Controllers\DBConsult\MonedaController')->getMoney();
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            if(count($result['content']) > 0){
                $money = $result['content'];
            }
        }
        return view('Empresa.Index', [
            'data_money'=>$money,
            'url_report'=>'http://localhost:8080/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2FERP_report&reportUnit=%2FERP_report%2Fempresa_report&standAlone=true&j_username=empresa_report&j_password=1234&sessionDecorator=no&idUsuario='.Auth::user()->id
        ]);
    }
}