<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ComprobanteController extends Controller{

    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request){
        $userID = 0;
        $companyID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            if($request->has('only_receipt')){
                if($request->only_receipt == 'true'){
                    $receiptID = 0;
                    if($request->has('receiptID')){
                        $receiptID = $request->receiptID;
                    }
                    return $this->getOnlyData($receiptID, $companyID, $userID);
                }
            }
            if($request->has('insert')){
                if($request->insert == 'true'){
                    $result = $this->getPrincipalMoney($request);
                    $result = json_decode(json_encode($result->getData()), true);
                    if(!$result['Success']){
                        response()->json(['Success'=>false, 'content'=>'Empresa no encontrada']);
                    }
                    if(count($result['content']) == 0){
                        response()->json(['Success'=>false, 'content'=>'Empresa no encontrada']);
                    }
                    $exchange = '';
                    $active = 1;
                    $registrationDate = date("Y/m/d");
                    $alternativeMoneyID = 0;
                    if($request->has('exchange')){
                        $exchange = $request->exchange;
                    }
                    if($request->has('alternativeMoneyID')){
                        $alternativeMoneyID = $request->alternativeMoneyID;
                    }
                    return $this->insert($exchange, $active, $registrationDate, $alternativeMoneyID, $companyID, $userID);
                }
            }
            $query = "SELECT co.IdComprobante as ID, co.Serie, CASE co.TipoComprobante WHEN 0 THEN 'Ingreso' WHEN 1 THEN 'Egreso' WHEN 2 THEN 'Traspaso' WHEN 3 THEN 'Apertura' WHEN 4 THEN 'Ajuste' END as 'Tipo', mo.Nombre as 'Moneda', DATE_FORMAT(co.Fecha, '%d/%m/%Y')as Fecha FROM comprobante as co, moneda as mo WHERE co.IdMoneda = mo.IdMoneda AND co.IdUsuario=? AND co.IdEmpresa=? ORDER BY co.IdComprobante DESC";
            $data=[$userID, $companyID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function insert($exchange, $active, $registrationDate, $alternativeMoneyID, $companyID, $userID){
        $principalMoneyID = 0;
        $inspectQuery = "SELECT Cambio, IdMonedaPrincipal, IdMonedaAlternativa FROM empresaMoneda WHERE IdUsuario=? AND IdEmpresa=? ORDER BY IdEmpresaMoneda DESC LIMIT 1";
        $inspectData = [$userID, $companyID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 1){
            $principalMoneyID = $inspectResult[0]->IdMonedaPrincipal;
            if($inspectResult[0]->IdMonedaAlternativa == $alternativeMoneyID && $inspectResult[0]->Cambio == $exchange){
                return response()->json(['Success'=>false, 'content'=>'No se permite registrar datos duplicados iguales al ultimo registro']);
            }
        }else{
            return response()->json(['Success'=>false, 'content'=>'Error, no se encontro ningun registro de la moneda']);
        }
        $updateQuery = "UPDATE empresaMoneda SET Activo = 0 WHERE IdUsuario=? AND IdEmpresa=? AND Activo = 1";
        $updateData = [$userID, $companyID];
        $updateResult = app('App\Http\Controllers\DBController')->update($updateQuery, $updateData);
        if($updateResult){
            $query = "INSERT INTO empresaMoneda (Cambio, Activo, FechaRegistro, IdEmpresa, IdMonedaPrincipal, IdMonedaAlternativa, IdUsuario) VALUES(?,?,?,?,?,?,?)";
            $data = [$exchange, $active, $registrationDate, $companyID, $principalMoneyID, $alternativeMoneyID, $userID];
            $result = app('App\Http\Controllers\DBController')->insert($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
    }
    public function getReceiptColumnName(){
        if(Auth::check()){
            $result = ['ID', 'Serie', 'Tipo', 'Moneda', 'Fecha'];
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }

    public function getDetailColumnName(){
        if(Auth::check()){
            $result = ['ID', 'Cuenta', 'Glosa', 'Debe', 'Haber'];
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }

    public function getOnlyData($receiptID, $companyID, $userID){
        $query = "SELECT c.Serie, DATE_FORMAT(c.Fecha, '%d/%m/%Y')as Fecha, CASE c.Estado WHEN 0 THEN 'Abierto' WHEN 1 THEN 'Cerrado' WHEN 2 THEN 'Anulado' END as Estado, c.TipoComprobante, c.TC, c.IdMoneda, c.Glosa FROM comprobante as c WHERE c.IdComprobante=? AND c.IdEmpresa=? AND c.IdUsuario=? LIMIT 1";
        $data = [$receiptID, $companyID, $userID];
        $result = app('App\Http\Controllers\DBController')->select($query, $data);
        $receipt = $result;
        if(count($receipt) > 0){
            $query = "SELECT dc.IdDetalleComprobante as ID,CONCAT(cu.Codigo,' ',cu.Nombre) as Cuenta, dc.Glosa, dc.MontoDebe as Debe, dc.MontoHaber as Haber FROM detallecomprobante as dc, cuenta as cu WHERE dc.IdCuenta = cu.IdCuenta AND dc.IdComprobante=? AND dc.IdUsuario=?";
            $data = [$receiptID, $userID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$receipt[0], 'content2'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Comprobante no encontrado']);
    }

    public function transaction(Request $request){
        return app('App\Http\Controllers\DBController')->transaction([$this, 'select'], $request);  
    }
}