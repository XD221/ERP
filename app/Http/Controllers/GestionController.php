<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GestionController extends Controller{
    public function index(Request $request){
        $columns = ['', ''];
        $data = [];
        $result = app('App\Http\Controllers\DBConsult\GestionController')->select($request);
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            if(count($result['content']) > 0){
                $columns = array_keys($result['content'][0]);
                $data = $result['content'];
            }else{
                $result = app('App\Http\Controllers\DBConsult\GestionController')->getColumnName();
                $result = json_decode(json_encode($result->getData()), true);
                if($result['Success']){
                    $columns = $result['content'];
                }
            }
        }
        return view('Gestion.Index',[
            'columns'=>$columns, 
            'data'=>$data,
            'url_report'=>'http://localhost:8080/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2FERP_report&reportUnit=%2FERP_report%2Fgestion_report&standAlone=true&j_username=gestion_report&j_password=1234&sessionDecorator=no&idUsuario='.Auth::user()->id.'&idEmpresa='.$request->session()->get('idEmpresa'),
        ]);
    }
}