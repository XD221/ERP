<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MonedaController extends Controller{
    public function index(Request $request){
        $columns = ['', ''];
        $data = [];
        $result = app('App\Http\Controllers\DBConsult\MonedaController')->select($request);
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            if(count($result['content']) > 0){
                $columns = array_keys($result['content'][0]);
                $data = $result['content'];
            }else{
                $result = app('App\Http\Controllers\DBConsult\MonedaController')->getColumnName();
                $result = json_decode(json_encode($result->getData()), true);
                if($result['Success']){
                    $columns = $result['content'];
                }
            }
        }
        $alternativeMoney = [];
        $result = app('App\Http\Controllers\DBConsult\MonedaController')->selectAlternative($request);
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            if(count($result['content']) > 0){
                $alternativeMoney = $result['content'];
            }
        }
        $request->request->add(['one_money'=> 'true']);
        $principalMoney = '';
        $alternativeMoneyID = 0;
        $exchange = 0;
        $result = app('App\Http\Controllers\DBConsult\MonedaController')->select($request);
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            $principalMoney = $result['content']['Nombre'];
            $alternativeMoneyID = is_null($result['content']['ID_Alternativa']) ? 0 : $result['content']['ID_Alternativa'];
            $exchange = $result['content']['Cambio'];
        }
        return view('Moneda.Index', [
            'columns'=>$columns, 
            'data'=>$data,
            'data_alternative_money'=>$alternativeMoney,
            'principal_money'=>$principalMoney,
            'alternative_money_id'=>$alternativeMoneyID,
            'money_exchange'=>$exchange,
        ]);
    }
}