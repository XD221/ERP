<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
    return view('Body.Index');
})->middleware(['userm:user']);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'LoginController@index')->name('login');
Route::get('/logout', 'LoginController@logout')->name('logout');
Route::get('/moneda', 'MonedaController@index')->name('moneda');
Route::get('/empresa', 'EmpresaController@index')->name('empresa');
Route::get('/gestion', 'GestionController@index')->name('gestion');
Route::get('/periodo', 'PeriodoController@index')->name('periodo');
Route::get('/cuenta', 'PlanCuentaController@index')->name('plan_cuenta');
Route::get('/comprobante', 'ComprobanteController@index')->name('comprobante');

Route::post('/', 'HomeController@addEmpresa');
Route::post('/login', 'LoginController@login');
Route::post('/moneda', 'DBConsult\MonedaController@transaction');
Route::post('/empresa', 'DBConsult\EmpresaController@transaction');
Route::post('/gestion', 'DBConsult\GestionController@transaction');
Route::post('/periodo', 'PeriodoController@index');
Route::post('/cuenta', 'DBConsult\CuentaController@transaction');
Route::post('/comprobante', 'DBConsult\ComprobanteController@transaction');