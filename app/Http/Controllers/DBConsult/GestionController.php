<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class GestionController extends Controller{

    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request){
        $userID = 0;
        $companyID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            if($request->has('insert')){
                if($request->insert == 'true'){
                    $name = '';
                    $fIni = '';
                    $fFin = '';
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    if($request->has('fIni')){
                        $fIni = $request->fIni;
                    }
                    if($request->has('fFin')){
                        $fFin = $request->fFin;
                    }
                    return $this->insert($name, $fIni, $fFin, $userID, $companyID);
                }
            }
            if($request->has('update')){
                if($request->update == 'true'){
                    $name = '';
                    $fIni = '';
                    $fFin = '';
                    $gestionID = 0;
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    if($request->has('fIni')){
                        $fIni = $request->fIni;
                    }
                    if($request->has('fFin')){
                        $fFin = $request->fFin;
                    }
                    if($request->has('gestionID')){
                        $gestionID = $request->gestionID;
                    }
                    return $this->update($name, $fIni, $fFin, $userID, $companyID, $gestionID);
                }
            }
            if($request->has('delete')){
                if($request->delete == 'true'){
                    $gestionID = 0;
                    if($request->has('gestionID')){
                        $gestionID = $request->gestionID;
                    }
                    return $this->delete($gestionID, $userID);
                }
            }
            $query = "SELECT IdGestion as ID, Nombre, DATE_FORMAT(FechaInicio, '%d/%m/%Y') as 'Fecha Inicio', DATE_FORMAT(FechaFin, '%d/%m/%Y') as 'Fecha Fin', CASE Estado WHEN 0 THEN 'Abierto' WHEN 1 THEN 'Cerrado' END as Estado FROM gestion WHERE (Estado=0 OR Estado=1)  AND IdUsuario=? AND IdEmpresa=? ORDER BY IdGestion DESC";
            $data=[$userID, $companyID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function insert($name, $fIni, $fFin, $userID, $companyID){
        $inspectQuery = "SELECT IdGestion FROM gestion WHERE IdUsuario=? AND IdEmpresa=? AND Estado=0";
        $inspectData = [$userID, $companyID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) < 2){
            $inspectQuery = "SELECT IdGestion FROM gestion WHERE Nombre=? AND IdUsuario=? AND IdEmpresa=?";
            $inspectData = [$name, $userID, $companyID];
            $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
            if(count($inspectResult) == 0){
                $inspectQuery = "SELECT FechaInicio as fini, FechaFin as ffin FROM gestion WHERE IdUsuario=? AND IdEmpresa=?";
                $inspectData = [$userID, $companyID];
                $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                foreach($inspectResult as $value){
                    if(($fIni >= $value->fini && $fIni <= $value->ffin) || ($fFin >= $value->fini && $fFin <= $value->ffin) || ($fIni < $value->fini && $fFin > $value->ffin)){
                        return response()->json(['Success'=>false, 'content'=>'No puede existir solapamiento entre gestiones']);
                    }
                }
                $query = "INSERT INTO gestion (Nombre, FechaInicio, FechaFin, Estado, IdUsuario, IdEmpresa) VALUES(?,?,?,0,?,?)";
                $data = [$name, $fIni, $fFin, $userID, $companyID];
                $result = app('App\Http\Controllers\DBController')->insert($query, $data);
                return response()->json(['Success'=>true, 'content'=>$result]);
            }
            return response()->json(['Success'=>false, 'content'=>'El nombre esta ocupado por una gestión']);
        }
        return response()->json(['Success'=>false, 'content'=>'No puede existir mas de 2 gestiones abiertas']);
    }
    public function update($name, $fIni, $fFin, $userID, $companyID, $gestionID){
        $inspectQuery = "SELECT IdGestion FROM gestion WHERE Nombre=? AND IdUsuario=? AND IdEmpresa=? AND IdGestion!=?";
        $inspectData = [$name, $userID, $companyID, $gestionID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            $inspectQuery = "SELECT IdGestion FROM gestion WHERE IdUsuario=? AND IdEmpresa=? AND IdGestion=? AND Estado=0 LIMIT 1";
            $inspectData = [$userID, $companyID, $gestionID];
            $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
            if(count($inspectResult) == 1){
                $inspectQuery = "SELECT FechaInicio as fini, FechaFin as ffin FROM gestion WHERE IdUsuario=? AND IdEmpresa=? AND IdGestion!=?";
                $inspectData = [$userID, $companyID, $gestionID];
                $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
                foreach($inspectResult as $value){
                    if(($fIni >= $value->fini && $fIni <= $value->ffin) || ($fFin >= $value->fini && $fFin <= $value->ffin) || ($fIni < $value->fini && $fFin > $value->ffin)){
                        return response()->json(['Success'=>false, 'content'=>'No puede existir solapamiento entre gestiones']);
                    }
                }
                $query = "UPDATE gestion SET Nombre=?, FechaInicio=?, FechaFin=? WHERE IdUsuario=? AND IdEmpresa=? AND IdGestion=?";
                $data = [$name, $fIni, $fFin, $userID, $companyID, $gestionID];
                $result = app('App\Http\Controllers\DBController')->update($query, $data);
                return response()->json(['Success'=>true, 'content'=>$result]);
            }
            return response()->json(['Success'=>false, 'content'=>'No se puede modificar, la gestión esta cerrada']);
        }
        return response()->json(['Success'=>false, 'content'=>'El nombre esta ocupado por una gestión']);
    }

    public function delete($gestionID, $userID){
        $inspectQuery = "SELECT IdPeriodo FROM periodo WHERE IdGestion=? AND IdUsuario=?";
        $inspectData = [$gestionID, $userID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            $query = "DELETE FROM gestion WHERE IdGestion=? AND IdUsuario=?";
            $data = [$gestionID, $userID];
            $result = app('App\Http\Controllers\DBController')->delete($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        else{
            return response()->json(['Success'=>false, 'content'=>"La gestión no se puede eliminar, existen periodos de esta gestión"]);
        }
    }

    public function selectOnlyEnabled(Request $request){
        $userID = 0;
        $companyID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            $query = "SELECT IdGestion as ID, Nombre FROM gestion WHERE Estado=0 AND IdUsuario=? AND IdEmpresa=? ORDER BY IdGestion DESC";
            $data=[$userID, $companyID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }

    public function checkGestion(Request $request){
        $userID = 0;
        $companyID = 0;
        $id = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            if($request->has('id')){
                $id = $request->id;
            }
            
            $query = "SELECT * FROM gestion WHERE IdUsuario=? AND IdEmpresa=? AND IdGestion=? AND Estado=0 LIMIT 1";
            $data = [$userID, $companyID, $id];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>count($result)==1]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }

    public function getDate(Request $request){
        $userID = 0;
        $companyID = 0;
        $id = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            if($request->has('id')){
                $id = $request->id;
            }
            
            $query = "SELECT FechaInicio as fini, FechaFin as ffin FROM gestion WHERE IdUsuario=? AND IdEmpresa=? AND IdGestion=? LIMIT 1";
            $data = [$userID, $companyID, $id];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            if(count($result) == 1){
                return response()->json(['Success'=>true, 'content'=>$result[0]]);
            }
            return response()->json(['Success'=>false, 'content'=>'No existe la gestión']);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }

    public function getColumnName(){
        if(Auth::check()){
            $result = ['ID', 'Nombre', 'Fecha Inicio', 'Fecha Fin', 'Estado'];
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    
    public function transaction(Request $request){
        return app('App\Http\Controllers\DBController')->transaction([$this, 'select'], $request);  
    }
}