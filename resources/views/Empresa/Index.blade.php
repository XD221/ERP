@extends('Layouts.Basic')
@section('head')
<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<style>
    html, body {
        background-color: #152036;
    }
</style>
@endsection
@section('body')
<div class="row" id='content'>
    <div style="padding-top:35vh;" ></div>
    <empresa class=""
    frm_method='POST'
    frm_action='/'
    title='Empresa'
    title_add='Agregar Empresa'
    title_modify='Editar Empresa'
    opt_default='Seleccione una empresa'
    btn_add=''
    btn_modify=''
    btn_delete=''
    btn_close='Cerrar'
    ph_name=''
    ph_nit=''
    ph_acronym=''
    ph_ph_number=''
    ph_email=''
    ph_address=''
    et_name='Nombre'
    et_nit='NIT'
    et_acronym='Sigla'
    et_ph_number='Telefono'
    et_email='Correo'
    et_address='Dirección'
    et_level='Niveles'
    et_money='Moneda Principal'
    regex_email='^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'
    :name_maximum_digits='65'
    :nit_maximum_digits='15'
    :acronym_maximum_digits='8'
    :ph_number_maximum_digits='18'
    :email_maximum_digits='45'
    :address_maximum_digits='360'
    btn_accept_add='Agregar'
    btn_accept_modify='Modificar'
    mssg_error_select='Debe seleccionar una empresa primero'
    mssg_error_name_empty='El campo nombre no puede quedar vacío'
    mssg_error_nit_empty='El campo NIT no puede quedar vacío'
    mssg_error_acronym_empty='El campo Sigla no puede quedar vacío'
    mssg_error_email_invalid='El correo es invalido'
    mssg_success_add='Datos agregado exitosamente'
    mssg_success_modify='Datos modificado exitosamente'
    mssg_error_pr_money_noselect='En moneda principal debe seleccionar una moneda'
    delete_confirm_title='¿Deseas eliminar la empresa "name"?'
    delete_confirm_btn_cancel='Cancelar'
    delete_confirm_btn_next='Eliminar'
    delete_confirm_text_invalid='El nombre es invalido'
    delete_confirm_mssg_success='La empresa "name" fue eliminada exitosamente'
    delete_confirm_mssg_fail='Error: No se pudo eliminar la empresa'
    opt_pr_money_default='Seleccione una moneda'
    :data_money='{{ json_encode($data_money) }}'
     />
</div>
@endsection
@section('footer')
    <script type="text/javascript" src="{{ asset('js/vue.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
@endsection