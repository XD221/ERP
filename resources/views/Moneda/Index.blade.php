@extends('Layouts.Content')
@section('content')
<moneda
:data_alternative_money="{{ json_encode($data_alternative_money) }}"
:select_alternative_money="{{ json_encode($alternative_money_id) }}"
money_exchange="{{ json_encode($money_exchange) }}"
principal_money="{{ $principal_money }}"
et_principal_money='Moneda Principal'
et_alternative_money='Moneda Alternativa'
et_exchange='Cambio'
et_add_title='Agregar Moneda'
:exchange_maximum_digits='9'
tab_money='Gestión de monedas'
btn_close='Cerrar'
:columns="{{ json_encode($columns) }}"
:rows="{{ json_encode($data) }}"
table_name='Moneda'
:column_order='1'
mssg_error_exchange_empty='El campo del Cambio esta vacio'
mssg_error_exchange_invalid='El valor del campo Cambio es invalido'
mssg_error_exchange_number_zero='El cambio debe ser mayor a 0'
mssg_error_al_money_empty='Seleccione una moneda alternativa'
mssg_noselect_row='Debe seleccionar una fila'
mssg_fail='Ah ocurrido un error al procesar la información'
opt_alternative_money_default='Seleccione una moneda'
:check_order_desc='true'
:check_remove_bfilter='true'
/>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pageTitle').text('Monedas');
    });
</script>
@endsection