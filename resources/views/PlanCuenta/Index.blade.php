@extends('Layouts.Content')
@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <plan_cuenta
        :material="{{ json_encode($data) }}"
        modal_title="Agregar nueva cuenta"
        btn_close="Cerrar"
        btn_accept_add='Agregar'
        btn_accept_modify='Modificar'
        :limit="{{ $limit }}"
        et_name="Nombre"
        :name_maximum_digits="45"
        mssg_error_limit_exceeded="Ha ocurrido un error, limite excedido"
        mssg_error_name_empty="El campo del Nombre esta vacio"
        mssg_noselect_row="Debe seleccionar una fila"
        mssg_disabled_delete="No se puede eliminar"
        mssg_success_add='Datos agregado exitosamente'
        mssg_success_modify='Datos modificado exitosamente'
        mssg_delete_confirm_success='La cuenta "name" fue eliminada exitosamente'
        mssg_delete_confirm_fail='Error: No se pudo eliminar la cuenta'
        delete_confirm_title='¿Deseas eliminar la cuenta "name"?'
        delete_confirm_btn_cancel='Cancelar'
        delete_confirm_btn_next='Eliminar'
        />
    </div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pageTitle').text('Plan de cuenta');
        $("meta[name='url_report']").attr("content", '{!! $url_report !!}');
    });
</script>
@endsection