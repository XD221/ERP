<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ComprobanteController extends Controller{
    public function index(Request $request){
        $receipt_columns = [' ',' '];
        $detail_columns = [' ',' '];
        $receipt_data = [];
        $data_company_money = [];
        $result = app('App\Http\Controllers\DBConsult\ComprobanteController')->select($request);
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            if(count($result['content']) > 0){
                $receipt_columns = array_keys($result['content'][0]);
                $receipt_data = $result['content'];
            }else{
                $result = app('App\Http\Controllers\DBConsult\ComprobanteController')->getReceiptColumnName();
                $result = json_decode(json_encode($result->getData()), true);
                if($result['Success']){
                    $receipt_columns = $result['content'];
                }
            }
        }
        $result = app('App\Http\Controllers\DBConsult\ComprobanteController')->getDetailColumnName();
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            $detail_columns = $result['content'];
        }
        $result = app('App\Http\Controllers\DBConsult\MonedaController')->getMoneyOfCompany($request);
        $result = json_decode(json_encode($result->getData()), true);
        if($result['Success']){
            if(count($result['content']) > 0){
                $data_company_money = $result['content'];
            }
        }
        return view('Comprobante.Index', [
            'receipt_columns'=>$receipt_columns, 
            'receipt_data'=>$receipt_data,
            'detail_columns'=>$detail_columns, 
            'data_company_money'=>$data_company_money, 
        ]);
    }
}