<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class MonedaController extends Controller{

    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function select(Request $request){
        $userID = 0;
        $companyID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            if($request->has('one_money')){
                if($request->one_money == 'true'){
                    return $this->getMoneyDatas($companyID, $userID);
                }
            }
            if($request->has('insert')){
                if($request->insert == 'true'){
                    $result = $this->getMoneyDatas($companyID, $userID);
                    $result = json_decode(json_encode($result->getData()), true);
                    if(!$result['Success']){
                        response()->json($result);
                    }
                    $exchange = '';
                    $active = 1;
                    $registrationDate = date("Y/m/d");
                    $alternativeMoneyID = 0;
                    if($request->has('exchange')){
                        $exchange = $request->exchange;
                    }
                    if($request->has('alternativeMoneyID')){
                        $alternativeMoneyID = $request->alternativeMoneyID;
                    }
                    return $this->insert($exchange, $active, $registrationDate, $alternativeMoneyID, $companyID, $userID);
                }
            }
            $query = "SELECT e.IdEmpresaMoneda as ID, DATE_FORMAT(e.FechaRegistro, '%d/%m/%Y') as 'Fecha de Registro', (SELECT Nombre FROM moneda WHERE IdMoneda=e.IdMonedaPrincipal LIMIT 1) as 'Moneda Principal', IFNULL((SELECT Nombre FROM moneda WHERE IdMoneda=e.IdMonedaAlternativa LIMIT 1), '-') as 'Moneda Alternativa', IF(e.Cambio > 0, e.Cambio , '-') as 'Cambio', CASE e.Activo WHEN 0 THEN 'Inactivo' WHEN 1 THEN 'Seleccionado' END as 'Activo' FROM empresaMoneda as e  WHERE e.IdUsuario=? AND e.IdEmpresa=? ORDER BY e.IdEmpresaMoneda DESC";
            $data=[$userID, $companyID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function insert($exchange, $active, $registrationDate, $alternativeMoneyID, $companyID, $userID){
        $principalMoneyID = 0;
        $inspectQuery = "SELECT Cambio, IdMonedaPrincipal, IdMonedaAlternativa FROM empresaMoneda WHERE IdUsuario=? AND IdEmpresa=? ORDER BY IdEmpresaMoneda DESC LIMIT 1";
        $inspectData = [$userID, $companyID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 1){
            $principalMoneyID = $inspectResult[0]->IdMonedaPrincipal;
            if($inspectResult[0]->IdMonedaAlternativa == $alternativeMoneyID && $inspectResult[0]->Cambio == $exchange){
                return response()->json(['Success'=>false, 'content'=>'No se permite registrar datos duplicados iguales al ultimo registro']);
            }
        }else{
            return response()->json(['Success'=>false, 'content'=>'Error, no se encontro ningun registro de la moneda']);
        }
        $updateQuery = "UPDATE empresaMoneda SET Activo = 0 WHERE IdUsuario=? AND IdEmpresa=? AND Activo = 1";
        $updateData = [$userID, $companyID];
        $updateResult = app('App\Http\Controllers\DBController')->update($updateQuery, $updateData);
        if($updateResult){
            $query = "INSERT INTO empresaMoneda (Cambio, Activo, FechaRegistro, IdEmpresa, IdMonedaPrincipal, IdMonedaAlternativa, IdUsuario) VALUES(?,?,?,?,?,?,?)";
            $data = [$exchange, $active, $registrationDate, $companyID, $principalMoneyID, $alternativeMoneyID, $userID];
            $result = app('App\Http\Controllers\DBController')->insert($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
    }
    public function getColumnName(){
        if(Auth::check()){
            $result = ['ID', 'Cambio', 'Activo', 'Fecha de Registro', 'Moneda Principal', 'Moneda Alternativa'];
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function selectAlternative(Request $request){
        $companyID = 0;
        if(Auth::check()){
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            $query = 'SELECT m.IdMoneda as ID, m.Nombre from moneda as m, empresaMoneda as em WHERE em.IdMonedaPrincipal!=m.IdMoneda AND em.IdEmpresa=? GROUP BY m.Nombre';
            $data = [$companyID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
    }
    public function getMoneyDatas($companyID, $userID){
        $query = 'SELECT m.IdMoneda as ID, m.Nombre, em.IdMonedaAlternativa as "ID_Alternativa", em.Cambio from moneda as m, empresaMoneda as em WHERE em.IdMonedaPrincipal=m.IdMoneda AND em.IdEmpresa=? AND em.IdUsuario=? ORDER BY em.IdEmpresaMoneda DESC LIMIT 1';
        $data = [$companyID, $userID];
        $result = app('App\Http\Controllers\DBController')->select($query, $data);
        if(count($result) > 0){
            return response()->json(['Success'=>true, 'content'=>$result[0]]);
        }
        return response()->json(['Success'=>false, 'content'=>'Empresa no encontrada']);
    }
    public function getMoney(){
        $userID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            $query = 'SELECT IdMoneda as ID, Nombre from moneda WHERE IdUsuario=?';
            $data = [$userID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
    }
    public function getMoneyOfCompany(Request $request){
        $userID = 0;
        $companyID = 0;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->session()->has('idEmpresa')){
                $companyID = $request->session()->get('idEmpresa');
            }
            $query = 'SELECT m.IdMoneda as ID, m.Nombre FROM moneda as m INNER JOIN (SELECT e.IdMonedaPrincipal, e.IdMonedaAlternativa FROM empresamoneda as e WHERE e.IdEmpresa=? AND e.IdUsuario=? ORDER BY e.IdEmpresaMoneda DESC LIMIT 1) as em ON (m.IdMoneda = em.IdMonedaPrincipal OR m.IdMoneda = em.IdMonedaAlternativa)';
            $data = [$companyID, $userID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
    }

    public function transaction(Request $request){
        return app('App\Http\Controllers\DBController')->transaction([$this, 'select'], $request);  
    }
}