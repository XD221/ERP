<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class EmpresaController extends Controller{

    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function selectName(Request $request){
        $userID = 0;
        $update = false;
        if(Auth::check()){
            $userID = Auth::user()->id;
            if($request->has('insert')){
                if($request->insert == 'true'){
                    $principalMoneyID = 0;
                    if($request->has('principalMoneyID')){
                        $principalMoneyID = $request->principalMoneyID;
                    }
                    $name = '';
                    if($request->has('name')){
                        $name = $request->name;
                    }
                    $nit = '';
                    if($request->has('nit')){
                        $nit = $request->nit;
                    }
                    $acronym = '';
                    if($request->has('acronym')){
                        $acronym = $request->acronym;
                    }
                    $ph_number = '';
                    if($request->has('ph_number')){
                        $ph_number = ($request->ph_number == '') ? '' : $request->ph_number;
                    }
                    $email = '';
                    if($request->has('email')){
                        $email = ($request->email == '') ? '' : $request->email;
                    }
                    $address = '';
                    if($request->has('address')){
                        $address = ($request->address == '') ? '' : $request->address;
                    }
                    $level = '3';
                    if($request->has('level')){
                        $level = intval($request->level);
                    }
                    return $this->insert($name, $nit, $acronym, $ph_number, $email, $address, $level, $userID, $principalMoneyID);
                }
            }
            if($request->has('id')){
                $id = $request->id;
                if($request->has('update')){
                    if($request->update == 'true'){
                        $principalMoneyID = 0;
                        if($request->has('principalMoneyID')){
                            $principalMoneyID = $request->principalMoneyID;
                        }
                        $name = '';
                        if($request->has('name')){
                            $name = $request->name;
                        }
                        $nit = '';
                        if($request->has('nit')){
                            $nit = $request->nit;
                        }
                        $acronym = '';
                        if($request->has('acronym')){
                            $acronym = $request->acronym;
                        }
                        $ph_number = '';
                        if($request->has('ph_number')){
                            $ph_number = ($request->ph_number == '') ? '' : $request->ph_number;
                        }
                        $email = '';
                        if($request->has('email')){
                            $email = ($request->email == '') ? '' : $request->email;
                        }
                        $address = '';
                        if($request->has('address')){
                            $address = ($request->address == '') ? '' : $request->address;
                        }
                        $level = '3';
                        if($request->has('level')){
                            $level = intval($request->level);
                        }
                        return $this->updateById($id, $userID, $name, $nit, $acronym, $ph_number, $email, $address, $level, $principalMoneyID);
                    }
                }
                if($request->has('delete')){
                    if($request->delete == 'true'){
                        return $this->delete($id, $userID);
                    }
                }
                return $this->selectById($id, $userID);
            }
            $query = "SELECT IdEmpresa as id, Nombre FROM empresa WHERE IdUsuario=? AND Estado=0";
            $data = [$userID];
            $result = app('App\Http\Controllers\DBController')->select($query, $data);
            return response()->json(['Success'=>true, 'content'=>$result]);
        }
        return response()->json(['Success'=>false, 'content'=>'Acceso no autorizado']);
    }
    public function selectById($id, $userID){
        $moneyValid = false;
        $moneyID=[];
        $query = "SELECT IdMoneda FROM empresaMoneda as em, moneda as m WHERE em.IdMonedaPrincipal=m.IdMoneda AND em.IdEmpresa=? AND em.IdUsuario=?";
        $data = [$id, $userID];
        $result = app('App\Http\Controllers\DBController')->select($query, $data);
        if(count($result) == 1){
            $moneyValid = true;
            $moneyID = $result[0]->IdMoneda;
        }else if(count($result) > 1){
            $moneyID = $result[0]->IdMoneda;
        }
        $query = "SELECT e.IdEmpresa as id, e.Nombre as Nombre, e.NIT as NIT, e.Sigla as Sigla, e.Telefono as Telefono, e.Correo as Correo, e.Direccion as Direccion, e.Niveles as Niveles, CASE WHEN COUNT(c.IdCuenta)>0 THEN true ELSE false END as tieneCuenta FROM empresa as e LEFT JOIN cuenta as c ON e.IdUsuario = c.IdUsuario AND c.IdEmpresa = e.IdEmpresa  WHERE e.IdEmpresa=? AND e.IdUsuario=? AND e.Estado=0 GROUP BY e.IdEmpresa LIMIT 1";
        $data = [$id, $userID];
        $result = app('App\Http\Controllers\DBController')->select($query, $data);
        return response()->json(['Success'=>true, 'content'=>$result[0], 'moneyValid'=>$moneyValid, 'moneyID'=>$moneyID]);
    }
    public function updateById($id, $userID, $name, $nit, $acronym, $ph_number, $email, $address, $level, $principalMoneyID){
        $inspectQuery = "SELECT * FROM empresa WHERE (Nombre=? OR NIT=? OR Sigla=?) AND IdUsuario=? AND IdEmpresa!=? AND estado=0";
        $inspectData = [$name, $nit, $acronym, $userID, $id];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            $inspectQuery = "SELECT CASE WHEN COUNT(c.IdCuenta)>0 THEN true ELSE false END as tieneCuenta FROM empresa as e, cuenta as c WHERE e.Niveles != ? AND e.IdUsuario=? AND e.IdEmpresa=? AND c.IdEmpresa=e.IdEmpresa AND e.Estado=0 LIMIT 1";
            $inspectData = [$level, $userID, $id];
            $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
            if(count($inspectResult) == 1){
                if(!$inspectResult[0]->tieneCuenta){
                    $query = "SELECT IdMoneda FROM empresaMoneda as em, moneda as m WHERE em.IdMonedaPrincipal=m.IdMoneda AND em.IdEmpresa=? AND em.IdUsuario=?";
                    $data = [$id, $userID];
                    $result = app('App\Http\Controllers\DBController')->select($query, $data);
                    if(count($result) == 1){
                        $query = "UPDATE empresaMoneda SET 	IdMonedaPrincipal=? WHERE IdEmpresa=? AND IdUsuario=?";
                        $data = [$principalMoneyID, $id, $userID];
                        $result = app('App\Http\Controllers\DBController')->update($query, $data);
                    }
                    $query = "UPDATE empresa SET Nombre=?, NIT=?, Sigla=?, Telefono=?, Correo=?, Direccion=?, Niveles=? WHERE IdEmpresa=? AND IdUsuario=?";
                    $data = [$name, $nit, $acronym, $ph_number, $email, $address, $level, $id, $userID];
                    $result = app('App\Http\Controllers\DBController')->update($query, $data);
                    return response()->json(['Success'=>true, 'content'=>$result]);
                }
                return response()->json(['Success'=>false, 'content'=>'El nivel no se puede alterar, existen cuentas en la empresa']);
            }
            return response()->json(['Success'=>false, 'content'=>'La empresa no existe']);
        }
        return response()->json(['Success'=>false, 'content'=>'Ya existe una empresa registrada con esas caracteristicas']);
    }

    public function insert($name, $nit, $acronym,  $ph_number, $email, $address, $level, $userID, $principalMoneyID){
        $inspectQuery = "SELECT * FROM empresa WHERE (Nombre=? OR NIT=? OR Sigla=?) AND IdUsuario=? AND estado=0";
        $inspectData = [$name, $nit, $acronym, $userID];
        $inspectResult = app('App\Http\Controllers\DBController')->select($inspectQuery, $inspectData);
        if(count($inspectResult) == 0){
            $data = array('Nombre'=>$name, 'NIT'=>$nit, 'Sigla'=>$acronym, 'Telefono'=>$ph_number, 'Correo'=>$email, 'Direccion'=>$address, 'Niveles'=>$level, 'Estado'=>0, 'IdUsuario'=>$userID);
            $result = app('App\Http\Controllers\DBController')->insertGetId('empresa', $data);
            if($result > 0){
                $companyID = $result;
                $query = "INSERT INTO empresaMoneda (Cambio, Activo, FechaRegistro, IdEmpresa, IdMonedaPrincipal, IdUsuario) VALUES(?,?,?,?,?,?)";
                $data = [0, 1, date("Y/m/d"), $companyID, $principalMoneyID, $userID];
                $result = app('App\Http\Controllers\DBController')->insert($query, $data);
                $code_extend = '';
                for($i = 0; $i < (intval($level) - 1); $i++){
                    $code_extend .='.0';
                }
                $data = [
                    ['Codigo'=>'1'.$code_extend, 'Nombre'=>'Activo', 'Nivel'=>'1', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>null],
                    ['Codigo'=>'2'.$code_extend, 'Nombre'=>'Pasivo', 'Nivel'=>'1', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>null],
                    ['Codigo'=>'3'.$code_extend, 'Nombre'=>'Patrimonio', 'Nivel'=>'1', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>null],
                    ['Codigo'=>'4'.$code_extend, 'Nombre'=>'Ingresos', 'Nivel'=>'1', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>null],
                    ['Codigo'=>'5'.$code_extend, 'Nombre'=>'Egresos', 'Nivel'=>'1', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>null],
                ];
                foreach($data as $key => $val){
                    $result = app('App\Http\Controllers\DBController')->insertGetId('cuenta', $val);
                }
                if($result > 0){
                    $inx = strpos($code_extend, '.0', 1);
                    $code_extend = substr($code_extend,($inx > 0) ? $inx : 0);
                    $data = [
                        ['Codigo'=>'5.1'.$code_extend, 'Nombre'=>'Costos', 'Nivel'=>'2', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>strval($result)],
                        ['Codigo'=>'5.2'.$code_extend, 'Nombre'=>'Gastos', 'Nivel'=>'2', 'TipoCuenta'=>'0', 'IdUsuario'=>strval($userID), 'IdEmpresa'=>strval($companyID), 'IdCuentaPadre'=>strval($result)],
                    ];
                    foreach($data as $key => $val){
                        $result = app('App\Http\Controllers\DBController')->insertGetId('cuenta', $val);
                    }
                }
                return response()->json(['Success'=>true, 'content'=>true]);
            }
        }
        return response()->json(['Success'=>false, 'content'=>'Ya existe una empresa registrada con esas caracteristicas']);
    }

    public function checkEmpresa($id, $userID){
        $query = "SELECT * FROM empresa WHERE IdEmpresa=? AND IdUsuario=? LIMIT 1";
        $data = [$id,$userID];
        $result = app('App\Http\Controllers\DBController')->select($query, $data);
        return $result;
    }

    public function delete($id, $userID){
        $query = "UPDATE empresa SET Estado=1 WHERE IdEmpresa=? AND IdUsuario=?";
        $data = [$id, $userID];
        $result = app('App\Http\Controllers\DBController')->update($query, $data);
        return response()->json(['Success'=>true, 'content'=>$result]);
    }
    
    public function transaction(Request $request){
        return app('App\Http\Controllers\DBController')->transaction([$this, 'selectName'], $request);  
    }
}