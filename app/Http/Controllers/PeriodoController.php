<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PeriodoController extends Controller{
    /**
     * Display a listing of the myformPost.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        if($request->isMethod('GET')){
            if($request->session()->has('idGestion')){
                $columns = ['', ''];
                $data = [];
                $result = app('App\Http\Controllers\DBConsult\PeriodoController')->transaction($request);
                $result = json_decode(json_encode($result->getData()), true);
                if($result['Success']){
                    if(count($result['content']) > 0){
                        $columns = array_keys($result['content'][0]);
                        $data = $result['content'];
                    }else{
                        $result = app('App\Http\Controllers\DBConsult\GestionController')->getColumnName();
                        $result = json_decode(json_encode($result->getData()), true);
                        if($result['Success']){
                            $columns = $result['content'];
                        }
                    }
                }
                return view('Periodo.Index',[
                    'columns'=>$columns, 
                    'data'=>$data,
                    'url_report'=>'http://localhost:8080/jasperserver/flow.html?_flowId=viewReportFlow&_flowId=viewReportFlow&ParentFolderUri=%2FERP_report&reportUnit=%2FERP_report%2Fperiodo_report&standAlone=true&j_username=periodo_report&j_password=1234&sessionDecorator=no&IdUsuario='.Auth::user()->id.'&IdEmpresa='.$request->session()->get('idEmpresa').'&IdGestion='.$request->session()->get('idGestion')
                ]);
            }
            return redirect('/gestion')->with(['status'=>'No selecciono una gestión', 'frame'=>'true']);
        }
        if($request->isMethod('POST')){
            if(!$request->has('id')){
                return app('App\Http\Controllers\DBConsult\PeriodoController')->transaction($request);
            }
            $name = '';
            if($request->has('name')){
                $name = $request->name;
            }
            $result = app('App\Http\Controllers\DBConsult\GestionController')->checkGestion($request);
            $result = json_decode(json_encode($result->getData()), true);
            if($result['Success']){
                if($result['content']){
                    $request->session()->put('idGestion', $request->id);
                    $request->session()->put('nomGestion', $name);
                    $result = app('App\Http\Controllers\DBConsult\GestionController')->getDate($request);
                    $result = json_decode(json_encode($result->getData()), true);
                    if($result['Success']){
                        $request->session()->put('gestion_fini', $result['content']['fini']);
                        $request->session()->put('gestion_ffin', $result['content']['ffin']);
                    }
                    return redirect('/periodo')->with(['status'=>'Gestion Seleccionada', 'frame'=>'true']);
                }
                return redirect('/gestion')->with(['status'=>'Gestión invalido', 'frame'=>'true']);
            }
            return redirect('/gestion')->with(['status'=>'Acceso no autorizado', 'frame'=>'true']);
        }
    }
}