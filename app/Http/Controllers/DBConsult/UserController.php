<?php

namespace App\Http\Controllers\DBConsult;

use App\Http\Controllers\Controller;

class UserController extends Controller{

    public function getUser($user, $pass){
        $query = "SELECT Nombre, Tipo FROM usuario WHERE Usuario=? AND Pass=? LIMIT 1";
        $data = [$user, $pass];
        $result = app('App\Http\Controllers\DBController')->select($query, $data);
        return $result[0];
    }
}