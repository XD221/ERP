import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
import VueTreeList from 'vue-tree-list';
import Tabs from 'vue-tabs-component';

window.Vue = require('vue').default;
Vue.use(VueSweetalert2);
Vue.use(VueTreeList);
Vue.use(Tabs);

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('login', require('./components/LoginComponent.vue').default);
Vue.component('empresa', require('./components/EmpresaComponent.vue').default);
Vue.component('home', require('./components/HomeComponent.vue').default);
Vue.component('gestion', require('./components/GestionComponent.vue').default);
Vue.component('periodo', require('./components/PeriodoComponent.vue').default);
Vue.component('custom_table', require('./components/CustomTableComponent.vue').default);
Vue.component('plan_cuenta', require('./components/PlanCuentaComponent.vue').default);
Vue.component('moneda', require('./components/MonedaComponent.vue').default);
Vue.component('comprobante', require('./components/ComprobanteComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#content',
});