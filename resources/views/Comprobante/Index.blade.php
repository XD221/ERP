@extends('Layouts.Content')
@section('css')
@endsection
@section('content')
<comprobante
:data_receipt_type="['Ingreso', 'Egreso', 'Transpaso', 'Apertura', 'Ajuste']"
:data_money="{{ json_encode($data_company_money) }}"
et_detail_serie='Serie'
et_detail_date='Fecha'
et_detail_status='Estado'
et_detail_receipt_type='Tipo de Comprobante'
et_detail_exchange='Cambio'
et_detail_money='Moneda'
et_detail_comment='Glosa'
et_receipt_account='Cuenta'
et_receipt_comment='Glosa'
et_receipt_debit='Debe'
et_receipt_credit='Haber'
et_detail_total='Totales'
opt_detail_receipt_type_default='Seleccione un tipo de comprobante'
opt_detail_money_default='Seleccione una moneda'
:receipt_rows='{{ json_encode($receipt_data) }}'
:receipt_columns='{{ json_encode($receipt_columns) }}'
receipt_table_name='comprobante'
:receipt_column_order='0'
:detail_columns='{{ json_encode($detail_columns) }}'
detail_table_name='detalle_comprobante'
:detail_column_order='0'
btn_close='Cerrar'
:receipt_check_remove_bfilter='true'
:detail_check_remove_bfilter='true'
mssg_noselect_row='Debe seleccionar una fila'
mssg_error_function_not_available='Esta funcion no esta disponible'
mssg_save='Datos guardado exitosamente'
mssg_question_save_data='¿Antes de salir, deseas guardar los datos?'
mssg_save_data_empty='No se puede guardar datos vacios'
mssg_save_data_exist='Los datos ya fueron guardados'
btn_mssg_save_close='No'
btn_mssg_save_confirm='Si'
></comprobante>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pageTitle').text('Comprobante');
        window.dateUpdate();
    });
</script>
@endsection