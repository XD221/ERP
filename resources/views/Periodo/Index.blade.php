@extends('Layouts.Content')
@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="breadcome-list">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="breadcomb-wp">
    					<div class="breadcomb-icon align-self-center">
    						<i class="icon nalika-home"></i>
    					</div>
    					<div class="breadcomb-ctn m-3">
                            <h5 class='text-white'>{{ date("d/m/Y", strtotime(Session::get('gestion_fini'))) }} -- {{ date("d/m/Y", strtotime(Session::get('gestion_ffin'))) }}</h5>
    					</div>
    				</div>
                </div>
            </div>
        </div>
        <periodo
        table_name='tperiodo'
        title_add='Agregar Periodo'
        title_modify='Editar Periodo'
        :name_maximum_digits='35'
        et_name='Nombre'
        et_start_date='Fecha Inicio'
        et_end_date='Fecha Fin'
        date_min="{{ Session::get('gestion_fini') }}"
        date_max="{{ Session::get('gestion_ffin') }}"
        :columns="{{ json_encode($columns) }}"
        :rows="{{ json_encode($data) }}"
        btn_return_periodo='Volver a Gestión'
        btn_return_periodo_href='/periodo/return'
        btn_accept_add='Agregar'
        btn_accept_modify='Editar'
        btn_close='Cerrar'
        mssg_noSelect_row='Debe seleccionar una fila'
        mssg_error_name_empty='El campo del Nombre esta vacio'
        mssg_error_fini_empty='El campo de la fecha Inicio esta vacio'
        mssg_error_ffin_empty='El campo de la fecha Fin esta vacio'
        mssg_error_date_invalid='La fecha inicio no puede ser mayor a la fecha fin'
        mssg_error_fini_format_invalid='El formato de la fecha Inicio es invalido'
        mssg_error_ffin_format_invalid='El formato de la fecha Fin es invalido'
        mssg_success_add='Datos agregado exitosamente'
        mssg_success_modify='Datos modificado exitosamente'
        mssg_delete_confirm_title='¿Deseas Eliminar el periodo "name"?'
        mssg_delete_confirm_success='El Periodo "name" fue eliminado exitosamente'
        mssg_fail='Ah ocurrido un error al procesar la información'
        :column_order='2'
        delete_confirm_btn_cancel='Cancelar'
        delete_confirm_btn_next='Eliminar'
        />
    </div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pageTitle').text('Periodo - {!! Session::get('nomGestion') !!}');
        $("meta[name='url_report']").attr("content", '{!! $url_report !!}');
    });
</script>
@endsection