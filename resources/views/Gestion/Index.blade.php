@extends('Layouts.Content')
@section('content')
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <gestion
        table_name='tcategory'
        title_add='Agregar Gestion'
        title_modify='Editar Gestion'
        show_periodo_form_action='/periodo'
        :name_maximum_digits='35'
        et_name='Nombre'
        et_start_date='Fecha Inicio'
        et_end_date='Fecha Fin'
        :columns="{{ json_encode($columns) }}"
        :rows="{{ json_encode($data) }}"
        btn_show_periodo='Ver Periodos'
        btn_accept_add='Agregar'
        btn_accept_modify='Editar'
        btn_close='Cerrar'
        mssg_noSelect_row='Debe seleccionar una fila'
        mssg_error_name_empty='El campo del Nombre esta vacio'
        mssg_error_fini_empty='El campo de la fecha Inicio esta vacio'
        mssg_error_ffin_empty='El campo de la fecha Fin esta vacio'
        mssg_error_date_invalid='La fecha inicio no puede ser mayor a la fecha fin'
        mssg_error_fini_format_invalid='El formato de la fecha Inicio es invalido'
        mssg_error_ffin_format_invalid='El formato de la fecha Fin es invalido'
        mssg_success_add='Datos agregado exitosamente'
        mssg_success_modify='Datos modificado exitosamente'
        mssg_show_periodo_only_open='Solo se pueden ver gestiones abiertas'
        mssg_delete_confirm_title='¿Deseas eliminar la gestión "name"?'
        mssg_delete_confirm_success='La Gestión "name" fue eliminada exitosamente'
        mssg_fail='Ah ocurrido un error al procesar la información'
        :column_order='2'
        delete_confirm_btn_cancel='Cancelar'
        delete_confirm_btn_next='Eliminar'
        />
    </div>
@endsection
@section('js')
<script type="text/javascript">
    $(document).ready(function(){
        $('#pageTitle').text('Gestión');
        $("meta[name='url_report']").attr("content", '{!! $url_report !!}');
        window.dateUpdate();
    });
</script>
@endsection