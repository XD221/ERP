@extends('Layouts.Content')
@section('content')
<div>
	<div class="breadcome-list">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-6 col-6">
				<div class="breadcomb-wp">
					<div class="breadcomb-icon">
						<i class="icon nalika-home"></i>
					</div>
					<div class="breadcomb-ctn m-3">
						<p class='text-uppercase' >Bienvenidos a <span class="bread-ntd">ERP</span></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<home
	></home>
</div>
@endsection
@section('js')
<script>
	$(document).ready(function(){
		$('#pageTitle').text('Home');
	});
</script>
@endsection