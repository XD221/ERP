@extends('Layouts.Main')
@section('content')
<div class="col-lg-12 col-md-12 col-sm-12 col-12">
    <div id='frame' ></div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#frame').load('/home?'+$.param({
            frame: 'true'
        }));

        $('#topbar-logo').click(function(){
            $('#frame').load('/home?'+$.param({
                frame: 'true'
            }));
        });
        
        $('.submenu-gestion').click(function(){
            $('#frame').load('/gestion?'+$.param({
                frame: 'true'
            }));
        });

        $('.submenu-plancuenta').click(function(){
            $('#frame').load('/cuenta?'+$.param({
                frame: 'true'
            }));
        });

        $('.submenu-moneda').click(function(){
            $('#frame').load('/moneda?'+$.param({
                frame: 'true'
            }));
        });
        $('.submenu-comprobante').click(function(){
            $('#frame').load('/comprobante?'+$.param({
                frame: 'true'
            }));
        });

    });
</script>
<script type="text/javascript" src="{{ asset('js/toastr.min.js') }}"></script>
    @yield('js')
@endsection