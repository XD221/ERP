@extends('Layouts.Basic')

@section('head')
    <!-- datatable -->
    <link rel="stylesheet" type="text/css" href="{{ asset('DataTables/datatables.min.css') }}" >
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    
	<!-- nalika Icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/nalika-icon.css')}}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/meanmenu.min.css')}}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/scrollbar/jquery.mCustomScrollbar.min.css')}}">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/metisMenu/metisMenu.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/metisMenu/metisMenu-vertical.css')}}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/responsive.css')}}">
    <!-- modernizr JS
		============================================ -->
    <script src="js/vendor/modernizr-2.8.3.min.js"></script>
@endsection
@section('body')
<div class="left-sidebar-pro">
    <nav id="sidebar" class="">
        <div class="sidebar-header" style="margin:6px;" >
            <a class='pointer' id='topbar-logo' ><img class="main-logo" src="{{asset('images/logo/logo.png')}}" width="100" height="30" alt="" /></a>
            <strong><img src="{{asset('images/logo/logosn.png')}}" width="60" height="10" alt="" /></strong>
        </div>
		<div class="nalika-profile">
			<div class="profile-dtl">
				<a><img src="{{asset('images/notification/4.png')}}" alt="" /></a>
				<h2 class='text-uppercase' >{{ Session::get('nomEmpresa') }}</h2>
			</div>
		</div>
        <div class="left-custom-menu-adp-wrap comment-scrollbar">
            <nav class="sidebar-nav left-sidebar-menu-pro">
                <ul class="metismenu" id="menu1">
                    <li>
                        <a class="has-arrow pointer">
						   <i class="icon nalika-home icon-wrap"></i>
						   <span class="mini-click-non">Contabilidad</span>
						</a>
                        <ul class="submenu-angle" aria-expanded="true">
                            <li class='submenu-comprobante' ><a class='pointer' title=""><span class="mini-sub-pro">Comprobante</span></a></li>
                            <li class="submenu-plancuenta" ><a class='pointer' title=""><span class="mini-sub-pro">Plan de Cuenta</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow pointer">
						   <i class="icon nalika-settings icon-wrap"></i>
						   <span class="mini-click-non">Configuración</span>
						</a>
                        <ul class="submenu-angle" aria-expanded="true">
                            <li class='submenu-gestion' ><a class='pointer' title=""><span class="mini-sub-pro">Gestión</span></a></li>
                            <li class='submenu-moneda' ><a class='pointer' title=""><span class="mini-sub-pro">Monedas</span></a></li>
                            <!--<li ><a title=""> <span class="mini-sub-pro">Dashboard v.3</span></a></li>-->
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </nav>
</div>
<!-- Start Welcome area -->
<div class="all-content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="logo-pro" style="padding: 6px; background-color: #2D529C;" >
                    <a href="/"><img class="main-logo" src="{{asset('images/logo/logo.png')}}" width="100" height="30" alt="" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="header-advance-area">
        <div class="header-top-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="header-top-wraper">
                            <div class="row">
                                <div class="col-lg-1 col-md-0 col-sm-1 col-12">
                                    <!--topbar left-->
                                </div>
                                <div class="col-9 row align-items-center">
                                    <h4 id='pageTitle' class='text-light text-center text-uppercase'></h4>
                                </div>
                                <div class="col-2">
                                    <div class="header-right-info">
                                        <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                            <li class="dropdown">
                                                <a class="dropdown-toggle" id="userToggle" data-bs-toggle="dropdown" aria-expanded="false">
													<i class="icon nalika-user"></i>
													<span class="admin-name text-uppercase">{{ auth()->user()->Nombre }}</span>
												</a>
                                                <ul class="dropdown-menu dropdown-header-top animated zoomIn" aria-labelledby="userToggle">
                                                    
                                                    <li>
                                                        <a href="/empresa"><span class="icon nalika-pie-chart author-log-ic"></span> Cambiar Empresa</a>
                                                        <a href="/logout"><span class="icon nalika-unlocked author-log-ic"></span> Cerrar Sesión</a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile Menu start -->
        <div class="mobile-menu-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="mobile-menu">
                            <nav id="dropdown">
                                <ul class="mobile-menu-nav">
                                    <li><a data-toggle="collapse" data-target="#Charts" href="#">Contabilidad <span class="admin-project-icon nalika-icon nalika-down-arrow"></span></a>
                                        <ul class="collapse dropdown-header-top">
                                            <li class='submenu-comprobante' ><a class='pointer' >Comprobante</a></li>
                                            <li class='submenu-plancuenta' ><a class='pointer' >Plan de Cuenta</a></li>
                                        </ul>
                                    </li>
                                    <li><a data-toggle="collapse" data-target="#Charts" href="#">Configuración <span class="admin-project-icon nalika-icon nalika-down-arrow"></span></a>
                                        <ul class="collapse dropdown-header-top">
                                            <li class='submenu-gestion' ><a class='pointer' >Gestión</a></li>
                                            <li class='submenu-moneda' ><a class='pointer' title=""><span class="mini-sub-pro">Monedas</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile Menu end -->
        <div class="breadcome-area">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>
    
    <div class="footer-copyright-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                        <p>Copyright © 2022 By DECX</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
    <!-- jquery
		============================================ -->
        <script src="{{asset('js/jquery.js')}}"></script>
    <!-- datatable -->
    <script type="text/javascript" src="{{asset('DataTables/datatables.min.js')}}"></script>
    <!-- meanmenu JS
		============================================ -->
    <script src="{{asset('js/jquery.meanmenu.js')}}"></script>
    <!-- mCustomScrollbar JS
		============================================ -->
    <script src="{{asset('js/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
    <script src="{{asset('js/scrollbar/mCustomScrollbar-active.js')}}"></script>
    <!-- metisMenu JS
		============================================ -->
    <script src="{{asset('js/metisMenu/metisMenu.min.js')}}"></script>
    <script src="{{asset('js/metisMenu/metisMenu-active.js')}}"></script>
    <!-- main JS
		============================================ -->
    <script src="{{asset('js/main.js')}}"></script>
    <!--custom-->
    @yield('scripts')
@endsection